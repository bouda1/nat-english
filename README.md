# nat-english

Here is a little game written for my daughter. The goal is to help her to learn English.
The goal is also for me to improve my Rust skills.

The software works with a text file given as argument. The file is structured of lines pairs, each pairs are
composed of a text written in french and its traduction in english.

Example:

```
Comment allez-vous ?
How are you?
La maison
The house
```
When the check is done, the ponctuation is removed, the text is also simplified to only keep one space between two words.
