extern crate reqwest;
extern crate url;
extern crate gtk;

use gtk::prelude::*;
use gtk::*;
use std::error::Error;
use std::fs::File;
use std::io::prelude::*;
use std::io::{Read, BufReader, stdin};
use url::form_urlencoded;
use std::path::Path;
use std::process::Command;
use std::process;
use std::collections::VecDeque;
use std::env;

struct Translate {
    pub fr: String,
    pub en: String,
}

fn parse_file(file: &String) -> VecDeque<Translate> {
    let mut retval = VecDeque::new();
    let mut lg = false;
    let mut fr = String::new();
    let mut en = String::new();
    let f = match File::open(file) {
        Err(e) => {
            eprintln!("Error: {}", e);
            process::exit(2);
        },
        Ok(f) => f
    };
    let file = BufReader::new(&f);
    for line in file.lines() {
        let l = line.unwrap();
        match lg {
          false => {
              fr = l;
              lg = true;
          },
          true => {
              en = l;
              lg = false;
          }
        }
        if !lg {
            retval.push_back(Translate{fr: fr.to_owned(), en: en.to_owned()});
        }
    }
    retval
}

fn build_url(txt: &str) -> String {
    let mut retval = "https://translate.google.com/translate_tts?ie=UTF-8&".to_owned();
    let complement = form_urlencoded::Serializer::new(String::new())
                    .append_pair("ie", "UTF-8")
                    .append_pair("q", txt)
                    .append_pair("tl", "en-US")
                    .append_pair("client", "tw-ob")
                    .finish();
    retval.push_str(&complement);
    retval
}

fn play(txt: &str) {
    let res = reqwest::get(&build_url(txt));
    let mut body = Vec::new();
    let mut res = match res {
        Ok(r) => r,
        Err(_) => {
            return ;
        }
    };
    let _ = res.read_to_end(&mut body);

    let path = Path::new("/tmp/foo.mp3");
    let display = path.display();
    let mut file = match File::create(&path) {
        Err(why) => panic!("couldn't create {}: {}", display, why.description()),
        Ok(file) => file,
    };

    match file.write_all(body.as_slice()) {
        Err(why) => panic!("couldn't write to {}: {}", display, why.description()),
        Ok(_) => {}
    }

    Command::new("mpg123").arg("/tmp/foo.mp3").output().expect("Failed to play sound");
}

fn clear_buffer(buf: &str) -> String {
    let mut retval = String::new();
    let mut space = true;
    for c in buf.chars() {
        match c {
            '!' => (),
            '?' => (),
            '.' => (),
            ',' => (),
            '$' => (),
            '-' => space = true,
            ' ' => space = true,
            '\n' => space = true,
            _ => {
                if space && !retval.is_empty() {
                    retval.push(' ');
                }
                space = false;
                retval.push_str(&c.to_lowercase().to_string());
            }
        }
    }
    retval
}

fn main() {
    if gtk::init().is_err() {
        eprintln!("Failed to initialize GTK.");
        process::exit(1);
    }

    let window = Window::new(WindowType::Toplevel);
    window.set_title("Nat English");
    window.set_default_size(350, 70);
    let message = Label::new("Welcome: ");
    let answer = Entry::new();
    answer.connect_activate(|t| {
        println!("enter pressed...{:?}", t.get_text());
    });
    let layout = Box::new(Orientation::Vertical, 2);
    layout.add(&message);
    layout.add(&answer);

    window.add(&layout);

    window.show_all();

    window.connect_delete_event(|_, _| {
        gtk::main_quit();
        Inhibit(false)
    });


    gtk::main();

    let file = env::args().nth(1);

    let file = match file {
        None => {
            eprintln!("No file provided.");
            process::exit(1);
        },
        Some(f) => f
    };
    let mut translation = parse_file(&file);
    let total = translation.len();
    let mut attempts = 0;

    loop {
        print!("{}[2J{}[H", 27 as char, 27 as char);
        let l = translation.pop_front();
        let ll = l.unwrap();
        attempts = attempts + 1;
        println!();
        println!("Essai {} sur {}", attempts, total);
        println!("====================================");
        println!("{}", &ll.fr);
        let mut buffer = String::new();
        let reader = stdin();
        reader.lock().read_line(&mut buffer).unwrap();

        play(&ll.en);
        if clear_buffer(&buffer) == clear_buffer(&ll.en) {
            println!("Bravo !");
        }
        else {
            println!("La bonne réponse est << {} >>", ll.en);
            translation.push_back(ll);
        }
        if translation.len() == 0 {
            break;
        }
    }
    println!("Tu as fait {} tentatives pour répondre à {} questions.",
             attempts, total);
}

#[cfg(test)]
mod tests {
    use *;

    #[test]
    fn clear_buffer1() {
        assert_eq!(clear_buffer("This is a test"), "this is a test");
    }

    #[test]
    fn clear_buffer2() {
        assert_eq!(clear_buffer("Hi. This is a test !?"), "hi this is a test");
    }

   #[test]
    fn clear_buffer3() {
        assert_eq!(clear_buffer("   Hi  ,    this is      a test     !?     "), "hi this is a test");
    }

   #[test]
    fn clear_buffer4() {
        assert_eq!(clear_buffer("   Hi  ,    this is   \n   a test\n     !?\n     "), "hi this is a test");
    }

}
